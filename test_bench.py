# -*- coding: utf-8 -*-
"""
Created on Sat Oct 10 11:24:38 2020

@author: biggg
"""

from hangman import makeMan


fid = open('test.txt','r')
ascii_art = fid.read()
fid.close()

count = 7
print(makeMan(count, ascii_art))

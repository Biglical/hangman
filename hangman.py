#!/usr/bin/python3
# Hangman game
#

# -----------------------------------
# Helper code
# You don't need to understand this helper code,
# but you will have to know how to use the functions
# (so be sure to read the docstrings!)

import random
import string
from PyDictionary import PyDictionary
import sys


def loadWords():
    """
    Returns a list of valid words. Words are strings of lowercase letters.

    Depending on the size of the word list, this function may
    take a while to finish.
    """
    # print("Loading word list from file...")
    # inFile: file
    inFile = open(WORDLIST_FILENAME, 'r')
    # line: string
    line = inFile.readline()
    # wordlist: list of strings
    wordlist = line.split()
    # print("  ", len(wordlist), "words loaded.")
    return wordlist


def chooseWord(wordlist):
    """
    wordlist (list): list of words (strings)

    Returns a word from wordlist at random
    """
    return random.choice(wordlist)

# end of helper code
# -----------------------------------

# Load the list of words into the variable wordlist
# so that it can be accessed from anywhere in the program


def isWordGuessed(secretWord, lettersGuessed):
    '''
    secretWord: string, the word the user is guessing
    lettersGuessed: list, what letters have been guessed so far
    returns: boolean, True if all the letters of secretWord are in lettersGuessed;
      False otherwise
    '''
    for letter in secretWord:
        if letter not in lettersGuessed:
            return False
    return True


def getGuessedWord(secretWord, lettersGuessed):
    '''
    secretWord: string, the word the user is guessing
    lettersGuessed: list, what letters have been guessed so far
    returns: string, comprised of letters and underscores that represents
      what letters in secretWord have been guessed so far.
    '''
    response = ''
    for letter in secretWord:
        if letter in lettersGuessed:
            response += letter
        else:
            response += '_ '
    return response


def getAvailableLetters(lettersGuessed):
    '''
    lettersGuessed: list, what letters have been guessed so far
    returns: string, comprised of letters that represents what letters have not
      yet been guessed.
    '''
    letters_left = ''
    letters = string.ascii_lowercase
    for letter in letters:
        if letter not in lettersGuessed:
            letters_left += letter
    return letters_left


def makeMan(count, *args):
    '''
    takes in a count 1-7 and returns a stickman with count+1 body parts

    Parameters
    ----------
    count : int
        Count of body parts.

    Returns
    -------
    Stick man.

    '''
    max_parts = 8
    template = list('    |    \n    0    \n   /|\/   \n   / \   \n')    
    if len(args) != 0:
        if args[0] != '':
            template = list(args[0])
    man = []

    for i, char in enumerate(template):
        if char != ' ' and char != '\n':
            template[i] = ' '
            man.append((i, char))
    man_length = len(man)
    part_size = divmod(man_length, max_parts)    
    if count > max_parts:
        count = max_parts
        

    for body_part in range(count+1):
        man_slice = slice(body_part*(part_size[0]),(body_part*(part_size[0])+part_size[0]))

        for p in man[man_slice]:
            template[p[0]]  = p[1]
        #template[man[man_slice][0]] = man[man_slice][1]
    return ''.join(template)


def hangman(secretWord, *args):
    '''
    secretWord: string, the secret word to guess.

    Starts up an interactive game of Hangman.

    * At the start of the game, let the user know how many
      letters the secretWord contains.

    * Ask the user to supply one guess (i.e. letter) per round.

    * The user should receive feedback immediately after each guess
      about whether their guess appears in the computers word.

    * After each round, you should also display to the user the
      partially guessed word so far, as well as letters that the
      user has not yet guessed.

    Follows the other limitations detailed in the problem write-up.
    '''
    ascii_art = ''
    if len(args) != 0:
        ascii_art = args[0]
    secretWord = chooseWord(wordlist)
    print('lets play a game of hangman')
    blank_list = ''
    for letter in secretWord:
        blank_list += '_ '
    print(blank_list)
    guess_count = 8

    print('you have ', guess_count, 'attempts')
    lettersGuessed = []
    man_count = 0
    while guess_count > 0:
        print('You have', guess_count,'left\n')
        letter_guessed_string = input('guess one or more letters\n').lower()
        for letter_guessed in letter_guessed_string:
            if letter_guessed not in string.ascii_lowercase:
                print(letter_guessed, 'is not a letter, jerk\n')
                continue
            if letter_guessed not in lettersGuessed:
                lettersGuessed.append(letter_guessed)
            else:
                print('letter', letter_guessed,
                      'has been guessed please pick another')
                continue
            print(getGuessedWord(secretWord, lettersGuessed))
            if isWordGuessed(secretWord, lettersGuessed) and guess_count >= 0:
                print('Congratulations')
                return 1
            else:
                if letter_guessed not in secretWord:
                    man_count += 1
                    guess_count += -1
                print(makeMan(man_count, ascii_art))
                print(getAvailableLetters(lettersGuessed))

    if guess_count <= 0:
        stars = ''
        for x in range(len(secretWord) + 2):
            stars += '*'
        print("                    " + stars)
        print("The secret word was *" + secretWord + '*')
        print("                    " + stars)
        print(dictionary.meaning(secretWord), '\n')
        return 0

dictionary = PyDictionary()
WORDLIST_FILENAME = "words.txt"
points = 0
if __name__ == "__main__":
    ascii_art = ''
    if len(sys.argv) == 2:
        new_art =  sys.argv[1]
        fid = open(new_art,'r')
        ascii_art = (fid.read())
        fid.close()
    while 1:
        try:
            while 1:
            
                wordlist = loadWords()
                secretWord = chooseWord(wordlist)
                if hangman(secretWord, ascii_art) == 1:
                    points += 1
                else:
                    points = 0
            
                print('Your current score is', points, '\n\n')
        except EOFError:
            break
        except KeyboardInterrupt:
            break        


# When you've completed your hangman function, uncomment these two lines
# and run this file to test! (hint: you might want to pick your own
# secretWord while you're testing)

# secretWord = chooseWord(wordlist).lower()
# hangman(secretWord)
